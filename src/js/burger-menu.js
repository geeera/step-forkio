const burger = document.querySelector('.burger__block');
const list = document.querySelector('.navbar__links');
const logo = document.querySelector('.navbar__logo--block');

burger.addEventListener('click', function () {
    list.classList.toggle('navbar__links--active');
    list.querySelectorAll('.navbar__link').forEach(item => item.classList.toggle('navbar__link--active'));
    this.querySelector('.burger--uniq').classList.toggle('burger--uniq--active');
    const burgerClose = this.querySelectorAll('.burger--close');
    console.log(burgerClose.firstChild);
    burgerClose[0].classList.toggle('burger--up');
    burgerClose[1].classList.toggle('burger--down');
});

logo.addEventListener('mouseover', function () {
    document.querySelector('.logo__svg').style.fill = 'url(#pattern1)';
    console.log(document.querySelector('.logo__svg').style.fill);
})

logo.addEventListener('mouseout', function () {
    document.querySelector('.logo__svg').style.fill = 'url(#pattern0)';
})